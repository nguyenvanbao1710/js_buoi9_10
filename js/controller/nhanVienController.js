function layThongTinTuForm() {
  var taiKhoanNv = document.getElementById("tknv").value;
  var tenNv = document.getElementById("name").value;
  var emailNv = document.getElementById("email").value;
  var matKhauNv = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCb = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;

  var nhanVien = new NhanVien(
    taiKhoanNv,
    tenNv,
    emailNv,
    matKhauNv,
    ngayLam,
    luongCb,
    chucVu,
    gioLam
  );
  return nhanVien;
}

function xuatDanhSachNhanVien(dsnv) {
  var contentHTML = "";

  for (var index = 0; index < dsnv.length; index++) {
    var nhanVien = dsnv[index];

    var contentTrTag = `<tr>
        <td>${nhanVien.taiKhoanNv}</td>
        <td>${nhanVien.tenNv}</td>
        <td>${nhanVien.emailNv}</td>
        <td>${nhanVien.ngayLam}</td>
        <td>${nhanVien.chucVu}</td>
        <td>${nhanVien.tongLuong()}</td>
        <td>${nhanVien.xepLoai()}</td>
        <td>  
    <button
    onclick="suaNhanVien('${nhanVien.taiKhoanNv}')"
    class="btn btn-success" data-toggle="modal"
    data-target="#myModal">Sửa</button>
    <button class="btn btn-danger"  onclick="xoaNhanVien('${
      nhanVien.taiKhoanNv
    }')">Xoá</button>
    </td>
    </tr>`;

    contentHTML += contentTrTag;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
//tableDanhSach

function xuatThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoanNv;
  document.getElementById("name").value = nv.tenNv;
  document.getElementById("email").value = nv.emailNv;
  document.getElementById("password").value = nv.matKhauNv;
  document.getElementById("luongCB").value = nv.luongCb;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
