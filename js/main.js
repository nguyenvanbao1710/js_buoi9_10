var danhSachNhanVien = [];

var validatorNv = new ValidatorNv();

const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoanNv == id;
  });
};

const luuLocalStorage = function () {
  var dsnvJson = JSON.stringify(danhSachNhanVien);

  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
};

// lấy dữ liệu từ localStorage khi user tải lại trang

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);

// gán cho array gốc và render lại giao diện

if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);
  danhSachNhanVien = danhSachNhanVien.map(function (item) {
    return new NhanVien(
      item.taiKhoanNv,
      item.tenNv,
      item.emailNv,
      item.matKhauNv,
      item.ngayLam,
      item.luongCb,
      item.chucVu,
      item.gioLam
    );
  });
  xuatDanhSachNhanVien(danhSachNhanVien);
}

function themNhanVien() {
  var newNhanVien = layThongTinTuForm();

  var isValid = true;

  var isValidTaiKhoanNv =
    validatorNv.kiemTraRong(
      "tknv",
      "tbTKNV",
      "Tài khoản nhân viên chưa nhập"
    ) &&
    validatorNv.kiemTraIdHopLe(newNhanVien, danhSachNhanVien) &&
    validatorNv.taiKhoanHopLe("tknv", "tbTKNV");

  var isValidEmail =
    validatorNv.kiemTraRong("email", "tbEmail", "Email chưa nhập") &&
    validatorNv.kiemTraEmail("email", "tbEmail");

  var isValidTenNv =
    validatorNv.kiemTraRong("name", "tbTen", "Họ và tên nhân viên chưa nhập") &&
    validatorNv.tenNhanVienHopLe("name", "tbTen");

  var isValidPassword =
    validatorNv.kiemTraRong("password", "tbMatKhau", "Mật khẩu chưa nhập") &&
    validatorNv.kiemTraMatKhau("password", "tbMatKhau");

  var isValidDate =
    validatorNv.kiemTraRong("datepicker", "tbNgay", "Ngày làm chưa nhập") &&
    validatorNv.kiemTraNgayLam("datepicker", "tbNgay");

  var isValidLuongCb =
    validatorNv.kiemTraRong("luongCB", "tbLuongCB", "Lương cơ bản chưa nhập") &&
    validatorNv.kiemTraLuongCb("luongCB", "tbLuongCB");

  var isValidChucVu = validatorNv.kiemTraChucVu("chucvu", "tbChucVu");

  var isValidGioLam =
    validatorNv.kiemTraRong("gioLam", "tbGiolam", "Giờ làm chưa nhập") &&
    validatorNv.kiemTraGioLamHopLe("gioLam", "tbGiolam");

  isValid =
    isValidTaiKhoanNv &&
    isValidTenNv &&
    isValidEmail &&
    isValidPassword &&
    isValidDate &&
    isValidLuongCb &&
    isValidChucVu &&
    isValidGioLam;

  if (isValid) {
    danhSachNhanVien.push(newNhanVien);
    xuatDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();

    // console.log(dsnvJson);
  }
  // console.log(danhSachNhanVien);
}

function xoaNhanVien(id) {
  var viTri = timKiemViTri(id, danhSachNhanVien);
  danhSachNhanVien.splice(viTri, 1);
  xuatDanhSachNhanVien(danhSachNhanVien);
  luuLocalStorage();
}

function suaNhanVien(id) {
  var viTri = timKiemViTri(id, danhSachNhanVien);
  var nhanVien = danhSachNhanVien[viTri];
  xuatThongTinLenForm(nhanVien);
}

function capNhatNhanVien() {
  var nhanVienEdit = layThongTinTuForm();
  let viTri = timKiemViTri(nhanVienEdit.taiKhoanNv, danhSachNhanVien);

  danhSachNhanVien[viTri] = nhanVienEdit;
  xuatDanhSachNhanVien(danhSachNhanVien);
  luuLocalStorage();
}

console.log(danhSachNhanVien);

function timNhanVien() {
  let loaiNV = document.getElementById("searchName").value;
  let nhanVienKha = danhSachNhanVien.filter((nv) => {
    return nv.gioLam >= 160 && nv.gioLam < 176;
  });
  let nhaVienTB = danhSachNhanVien.filter((nv) => {
    return nv.gioLam < 160;
  });
  let nhanVienGioi = danhSachNhanVien.filter((nv) => {
    return nv.gioLam >= 176 && nv.gioLam < 192;
  });
  let nhanVienXuatSac = danhSachNhanVien.filter((nv) => {
    return nv.gioLam >= 192;
  });

  if (loaiNV == "Nhân viên khá") {
    danhSachNhanVien = nhanVienKha;
    console.log(danhSachNhanVien);
  } else if (loaiNV == "Nhân viên giỏi") {
    danhSachNhanVien = nhanVienGioi;
  } else if (loaiNV == "Nhân viên trung bình") {
    danhSachNhanVien = nhaVienTB;
  } else if (loaiNV == "Nhân viên xuất sắc") {
    danhSachNhanVien = nhanVienXuatSac;
  }

  xuatDanhSachNhanVien(danhSachNhanVien);
}
