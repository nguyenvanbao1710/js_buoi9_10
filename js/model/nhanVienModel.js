var NhanVien = function (
  _taiKhoanNv,
  _tenNv,
  _emailNv,
  _matKhauNv,
  _ngayLam,
  _luongCb,
  _chucVu,
  _gioLam
) {
  this.taiKhoanNv = _taiKhoanNv;
  this.tenNv = _tenNv;
  this.emailNv = _emailNv;
  this.matKhauNv = _matKhauNv;
  this.ngayLam = _ngayLam;
  this.luongCb = _luongCb;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tongLuong = function () {
    let heSoLuong;
    if (this.chucVu == "Sếp") {
      heSoLuong = this.luongCb * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      heSoLuong = this.luongCb * 2;
    } else {
      heSoLuong = this.luongCb;
    }
    return heSoLuong;
  };
  this.xepLoai = function () {
    let xepLoaiNv;
    if (192 <= this.gioLam) {
      xepLoaiNv = "Nhân viên xuất sắc";
    } else if (176 <= this.gioLam && this.gioLam < 192) {
      xepLoaiNv = "Nhân viên giỏi";
    } else if (160 <= this.gioLam && this.gioLam < 176) {
      xepLoaiNv = "Nhân viên khá";
    } else if (this.gioLam < 160) {
      xepLoaiNv = "Nhân viên trung bình";
    }
    return xepLoaiNv;
  };
};
