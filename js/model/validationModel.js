function ValidatorNv() {
  // kiểm tra rỗng
  this.kiemTraRong = function (idTarget, idError, messageError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (!valueTarget) {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  // kiểm tra id không trùng
  this.kiemTraIdHopLe = function (newNhanVien, danhSachNhanVien) {
    var index = danhSachNhanVien.findIndex(function (item) {
      return item.taiKhoanNv == newNhanVien.taiKhoanNv;
    });
    if (index == -1) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    }
    document.getElementById("tbTKNV").innerText =
      "Tài khoản nhân viên không được trùng";
    return false;
  };
  // kiểm tra tài khoản hợp lệ
  this.taiKhoanHopLe = function (idTarget, idError) {
    let parten = /^[0-9]{4,6}$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Tên tài khoản phải từ 4 - 6 chữ số";
  };
  // kiểm tra tên nhân viên hợp lệ
  this.tenNhanVienHopLe = function (idTarget, idError) {
    let parten = /^[a-zA-Z]{4,}(?: [a-zA-Z]+){0,2}$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Tên nhân viên phải là chữ";
  };
  // kiểm tra email hợp lệ
  this.kiemTraEmail = function (idTarget, idError) {
    let parten = /^[a-z0-9](.?[a-z0-9]){5,}@g(oogle)?mail.com$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Email không hợp lệ";
  };
  // kiểm tra mật khẩu hợp lệ
  this.kiemTraMatKhau = function (idTarget, idError) {
    let parten =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Mật khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
  };
  // Kiểm tra ngày hợp lệ
  this.kiemTraNgayLam = function (idTarget, idError) {
    let parten = /^\d{2}\/\d{2}\/\d{4}$/;

    let valueInput = document.getElementById(idTarget).value;
    if (parten.test(valueInput)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Định dạng ngày dd/mm/yyyy";
  };
  // kiểm tra lương cơ bản hợp lệ
  this.kiemTraLuongCb = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (valueInput >= 1000000 && valueInput <= 20000000) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Lương cơ bản từ 1000000 đến 20000000";
  };
  // kiểm tra chức vụ hợp lệ
  this.kiemTraChucVu = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (
      valueInput == "Sếp" ||
      valueInput == "Trưởng phòng" ||
      valueInput == "Nhân viên"
    ) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Chức vụ không hợp lệ";
  };
  // kiểm tra giờ làm hợp lệ
  this.kiemTraGioLamHopLe = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value;
    if (valueInput >= 80 && valueInput <= 200) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText =
      "Giờ làm trong tháng từ 80 đến 200";
  };
}
